package com.firstProject.firstProject.Service;

import com.firstProject.firstProject.Entities.TimeSlot;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TimeSlotService
{
    private List<TimeSlot> allSlots = new ArrayList<>();

    public List<TimeSlot> getAllSlots() {
        return allSlots;
    }

    public void addSlots(TimeSlot timeSlot) {
        allSlots.add(timeSlot);
    }
}