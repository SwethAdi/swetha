package com.firstProject.firstProject.Controller;

import com.firstProject.firstProject.Entities.Days;
import com.firstProject.firstProject.Entities.Employee;
import com.firstProject.firstProject.Entities.Schedule;
import com.firstProject.firstProject.Entities.TimeSlot;
import com.firstProject.firstProject.Repositories.ScheduleRepository;
import com.firstProject.firstProject.Repositories.TimeSlotRepository;
import com.firstProject.firstProject.Repositories.UserRepository;
import com.firstProject.firstProject.Service.TimeSlotService;
import com.firstProject.firstProject.util.ApplicationUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


@Controller
public class ApplicationController {
    private final UserRepository userRepository;
    private final ScheduleRepository scheduleRepository;
    private final TimeSlotRepository timeSlotRepository;
    //private final ApplicationUtility appUtility = new ApplicationUtility();

    @Autowired
    public ApplicationController(UserRepository userRepository, ScheduleRepository scheduleRepository,TimeSlotRepository timeSlotRepository) {
        this.userRepository = userRepository;
        this.scheduleRepository = scheduleRepository;
        this.timeSlotRepository=timeSlotRepository;
    }

    @GetMapping("/signup")
    public String showSignUpForm(Employee employee, Schedule schedule) {

        return "emp";
    }

    @PostMapping("/adduser")
    public String addUser(@Valid Employee employee, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "emp";
        }

        userRepository.save(employee);
        model.addAttribute("employees", userRepository.findAll());
        Iterator timeslots12=  timeSlotRepository.findAll().iterator();

        while (null!=timeslots12&&timeslots12.hasNext()) {
            TimeSlot ts = (TimeSlot) timeslots12.next() ;
            System.out.print("test fir DB value" + ts.getFridayRadioName() + " ");
        }
        model.addAttribute("timeSlots", timeSlotRepository.findAll());
        //System.out.println();

        return "index";



    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Employee employee = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        model.addAttribute("employee", employee);
        return "update";
    }

    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") long id, @Valid Employee employee, BindingResult result, Model model) {
        if (result.hasErrors()) {
            employee.setId(id);
            return "update";
        }

        userRepository.save(employee);
        model.addAttribute("employees", userRepository.findAll());
        return "index";
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model) {
        Employee employee = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        userRepository.delete(employee);
        model.addAttribute("employees", userRepository.findAll());
        return "index";
    }

    @GetMapping("/greeting")
    public String greetingForm() {

        return "schedule";
    }


    @PostMapping("/greeting")
    public String submit(@RequestParam("date1")
                         @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date1,
                         @RequestParam("date2")  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date2,
                         Model model)

    {

        Schedule newSchedule = new Schedule();
        newSchedule.setDate1(date1);
        newSchedule.setDate2(date2);
        scheduleRepository.save(newSchedule);
       model.addAttribute("schedule",scheduleRepository.findAll());
        System.out.println("successful   " + date1);
        System.out.println(date2);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println("ttestdate "+dtf.format(now)); //2016/11/16 12:08:43
        model.addAttribute("date",dtf.format(now));
        //System.out.println(submit.getDate2());
        return "slot";

    }

    @PostMapping("/submitSchedule")
    public String submitSchedule(Model model,TimeSlot timeSlot,BindingResult result)
    {
        if (result.hasErrors()) {
            return "slot";
        }

        System.out.println(timeSlot.getMondayRadioName());
        System.out.println(timeSlot.getTuesdayRadioName());
        System.out.println(timeSlot.getWednesdayRadioName());
        System.out.println(timeSlot.getThursdayRadioName());
        System.out.println(timeSlot.getFridayRadioName());
        System.out.println(timeSlot.getSaturdayRadioName());
        System.out.println(timeSlot.getSundayRadioName());
        //model.addAttribute(timeSlot);

        //appUtility.saveTimeslot(timeSlot);
        //TimeSlot timeSlot1=new TimeSlot();
        //timeSlot1.setFridayRadioName("test1111");
        model.addAttribute("timeSlot", timeSlot);
        timeSlotRepository.save(timeSlot);
       model.addAttribute("timeslot",timeSlotRepository.findAll());

       // model.addAttribute()

        Iterator timeslots12=  timeSlotRepository.findAll().iterator();

        while (null!=timeslots12&&timeslots12.hasNext()) {
            TimeSlot ts = (TimeSlot) timeslots12.next() ;
            System.out.print("test fir DB value" + ts.getFridayRadioName() + " ");
        }
        //System.out.println();



        return "timeslot";
    }


    @GetMapping("/addschedule")
    public String addschedule(Model model) {
        model.addAttribute("timeSlot", new TimeSlot());
        return "index";
    }

    @PostMapping("/addschedule")
    public String saveSchedule(@ModelAttribute TimeSlot timeslot) {

        return "index";
    }
}