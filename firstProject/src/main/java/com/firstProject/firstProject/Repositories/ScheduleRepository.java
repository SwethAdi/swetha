package com.firstProject.firstProject.Repositories;

import com.firstProject.firstProject.Entities.Schedule;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface ScheduleRepository extends CrudRepository<Schedule, Long>
{
    List<Schedule>findById(String id);

}
