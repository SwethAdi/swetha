package com.firstProject.firstProject.Repositories;

import com.firstProject.firstProject.Entities.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<Employee, Long>
{
    List<Employee> findByName(String name);
}
