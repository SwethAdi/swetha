package com.firstProject.firstProject.Repositories;

import com.firstProject.firstProject.Entities.TimeSlot;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TimeSlotRepository extends CrudRepository<TimeSlot,Long> {
    //List<TimeSlot> findAll(String name);
}
