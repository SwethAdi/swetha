package com.firstProject.firstProject.Entities;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TimeSlot {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String mondayRadioName;
    private String tuesdayRadioName;
    private String wednesdayRadioName;
    private String thursdayRadioName;
    private String fridayRadioName;
    private String saturdayRadioName;
    private String sundayRadioName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TimeSlot(){

    }
    public TimeSlot(String mondayRadioName, String tuesdayRadioName, String wednesdayRadioName,
                    String thursdayRadioName,
                    String fridayRadioName, String saturdayRadioName, String sundayRadioName)
    {
        this.mondayRadioName = mondayRadioName;
        this.tuesdayRadioName = tuesdayRadioName;
        this.wednesdayRadioName = wednesdayRadioName;
        this.thursdayRadioName = thursdayRadioName;
        this.fridayRadioName = fridayRadioName;
        this.saturdayRadioName = saturdayRadioName;
        this.sundayRadioName = sundayRadioName;
    }

    public String getMondayRadioName() {

        return mondayRadioName;
    }

    public void setMondayRadioName(String mondayRadioName) {

        this.mondayRadioName = mondayRadioName;
    }

    public String getTuesdayRadioName() {
        return tuesdayRadioName;
    }

    public void setTuesdayRadioName(String tuesdayRadioName) {
        this.tuesdayRadioName = tuesdayRadioName;
    }

    public String getWednesdayRadioName() {
        return wednesdayRadioName;
    }

    public void setWednesdayRadioName(String wednesdayRadioName) {
        this.wednesdayRadioName = wednesdayRadioName;
    }

    public String getThursdayRadioName() {
        return thursdayRadioName;
    }

    public void setThursdayRadioName(String thursdayRadioName) {
        this.thursdayRadioName = thursdayRadioName;
    }

    public String getFridayRadioName() {
        return fridayRadioName;
    }

    public void setFridayRadioName(String fridayRadioName) {
        this.fridayRadioName = fridayRadioName;
    }

    public String getSaturdayRadioName() {
        return saturdayRadioName;
    }

    public void setSaturdayRadioName(String saturdayRadioName) {
        this.saturdayRadioName = saturdayRadioName;
    }

    public String getSundayRadioName() {
        return sundayRadioName;
    }

    public void setSundayRadioName(String sundayRadioName) {
        this.sundayRadioName = sundayRadioName;
    }
}
