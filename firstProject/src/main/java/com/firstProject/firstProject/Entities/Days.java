package com.firstProject.firstProject.Entities;

public class Days{
    private String gender;
    private String name;

    public Days(){
        
    }
    public Days(String gender, String name) {
        this.gender = gender;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Days(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}