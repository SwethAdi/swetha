package com.firstProject.firstProject.Entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Schedule {

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private Long id;

private LocalDate date1;
private LocalDate date2;

    public Schedule(){
    }

    public Schedule(LocalDate date1, LocalDate date2) {
        this.date1 = date1;
        this.date2 = date2;
    }

    public LocalDate getDate1() {
        return date1;
    }

    public void setDate1(LocalDate date1) {
        this.date1 = date1;
    }

    public LocalDate getDate2() {
        return date2;
    }

    public void setDate2(LocalDate date2) {
        this.date2 = date2;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "date1=" + date1 +
                ", date2=" + date2 +
                '}';
    }
}
